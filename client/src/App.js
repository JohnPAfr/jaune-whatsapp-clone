import './App.css';
import Chat from './Chat/Chat';
import Sidebar from './Sidebar/Sidebar';
import Pusher from 'pusher-js';
import { useEffect, useState } from 'react';
import axios from './axios';


function App() {
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    axios.get('/messages/sync')
      .then(res => {
        setMessages(res.data);
      })
  }, []);

  useEffect(() => {
    Pusher.logToConsole = true;

    const pusher = new Pusher('19d62f09f72a76cf776e', {
      cluster: 'eu'
    });
  
    const channel = pusher.subscribe('messages');
    channel.bind('inserted', (data) => {
      setMessages([...messages, data]);
    });

    return () => {
      channel.unbind_all()
      channel.unsubscribe();
    }
  }, [messages]);

  console.log(messages);
  

  return (
    <div className="app">
      <div className="app__body">
        <Sidebar />
        <Chat messages={messages}/>
      </div>
    </div>
  );
}

export default App;
