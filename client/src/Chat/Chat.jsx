import React from 'react'
import "./Chat.css"
import ChatBody from './ChatBody'
import ChatFooter from './ChatFooter'
import ChatHeader from './ChatHeader'

function Chat({messages}) {
  return (
    <div className="chat">
      <ChatHeader />
      <ChatBody messages={messages}/>
      <ChatFooter />
    </div>
  )
}

export default Chat
