import React from 'react'

function ChatBody({messages}) {
  return (
    <div className="chat__body">
      {messages.map(m => {
        return <p key={m.id}
        className={`chat__message + ${m.received === true ? ' chat__reciever' : ''}`}>
          <span className="chat__name">{m.name}</span>
          {m.message}
          <span className="chat__timestamp">
            {m.timestamp}
          </span>
        </p>
      })}
      {/* <p className="chat__message">
        <span className="chat__name">Name</span>
        This is a message
        <span className="chat__timestamp">
          {new Date().toUTCString()}
        </span>
      </p>

      <p className="chat__message chat__reciever">
        <span className="chat__name">Name</span>
        This is a message
        <span className="chat__timestamp">
          {new Date().toUTCString()}
        </span>
      </p>

      <p className="chat__message">
        <span className="chat__name">Name</span>
        This is a message
        <span className="chat__timestamp">
          {new Date().toUTCString()}
        </span>
      </p> */}
    </div>
  )
}

export default ChatBody
