import React, { useState } from 'react';
import InsertEmoticonIcon from "@material-ui/icons/InsertEmoticon";
import MicIcon from "@material-ui/icons/Mic";
import axios from '../axios';


function ChatFooter() {
  const [input, setInput] = useState('')

  const sendMessage = async (e) => {
    e.preventDefault();
    await axios.post('/messages/new', {
      message: input,
      name: 'Jaune',
      timestamp: new Date().toUTCString(),
      received: false
    });
    setInput('');
  }
  return (
    <div className="chat__footer">
      <InsertEmoticonIcon />
      <form onSubmit={sendMessage}>
        <input 
        type="text"
        value={input}
        onChange={(e) => setInput(e.target.value)}
        placeholder="Type a message"/>
        <button type="submit">Send message</button>
      </form>
      <MicIcon />
    </div>
  )
}

export default ChatFooter
