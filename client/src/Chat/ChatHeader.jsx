import React from 'react'
import DonutLargeIcon from '@material-ui/icons/DonutLarge';
import AttachFile from '@material-ui/icons/AttachFile';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { Avatar, IconButton } from "@material-ui/core"

function ChatHeader() {
  return (
    <div className="chat__header">
      <Avatar />
      <div className="chat__headerInfo">
        <h3>Room Name</h3>
        <p>Last seen at...</p>
      </div>
      <div className="chat__headerRight">
        <IconButton>
          <DonutLargeIcon />
        </IconButton>
        <IconButton>
          <AttachFile />
        </IconButton>
        <IconButton>
          <MoreVertIcon />
        </IconButton>
      </div>
    </div>
  )
}

export default ChatHeader
