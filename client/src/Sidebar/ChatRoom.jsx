import { Avatar } from '@material-ui/core'
import React from 'react'

function ChatRoom() {
  return (
    <div className="sidebar__chat">
      <Avatar />
      <div className="sidebar__chatName">
        <h2>Title</h2>
        <p>paragraph</p>
      </div>
    </div>
  )
}

export default ChatRoom
