import React from 'react'
import ChatRoom from './ChatRoom'


function ChatRooms() {
  return (
    <div className="sidebar__chats">
      <ChatRoom />
      <ChatRoom />
      <ChatRoom />
      <ChatRoom />
    </div>
  )
}

export default ChatRooms
