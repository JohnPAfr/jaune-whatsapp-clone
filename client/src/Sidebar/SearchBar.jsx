import React from 'react';
import { SearchOutlined } from '@material-ui/icons';


function SearchBar() {
  return (
    <div className="sidebar__search">
      <div className="sidebar__searchContainer">
        <SearchOutlined />
        <input type="text" placeholder="Search or start a new chat"/>
      </div>
    </div>
  )
}

export default SearchBar;
