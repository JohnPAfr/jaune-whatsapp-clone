import React from 'react'
import ChatRooms from './ChatRooms'
import SearchBar from './SearchBar'
import "./Sidebar.css"
import SidebarHeader from './SidebarHeader'

function Sidebar() {
  return (
    <div className="sidebar">
      <SidebarHeader />
      <SearchBar />
      <ChatRooms />
    </div>
  )
}

export default Sidebar
