import axios from 'axios';

const instance = axios.create({
  baseURL: "https://jaune-whatsapp.herokuapp.com/"
});

export default instance;