import express from 'express';
import mongoose from 'mongoose';
import Pusher from 'pusher';
import cors from 'cors';
import Messages from './dbMessages.js';

/**
 * admin
 * AtVt3978EnvNufgh
 */

 // mongodb+srv://admin:AtVt3978EnvNufgh@jaune-whatsapp-cluster.9gdfi.mongodb.net/whatsappDB?retryWrites=true&w=majority

 // Init

const app = express();
const PORT = process.env.PORT || 3333;

const pusher = new Pusher({
  appId: "1112338",
  key: "19d62f09f72a76cf776e",
  secret: "a58c69de6ddd0e6ba9d2",
  cluster: "eu",
  useTLS: true
});



// Db config

const connection_url = "mongodb+srv://admin:AtVt3978EnvNufgh@jaune-whatsapp-cluster.9gdfi.mongodb.net/whatsappDB?retryWrites=true&w=majority";

mongoose.connect(connection_url, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;

db.once('open', () => {
  const msgCollection = db.collection('messagecontents');
  const changeStream = msgCollection.watch();

  changeStream.on('change', (change) => {
    if(change.operationType === 'insert') {
      const messageDetails = change.fullDocument;
      pusher.trigger("messages", "inserted", {
        message: messageDetails.message,
        name: messageDetails.name,
        timestamp: messageDetails.timestamp,
        received: messageDetails.received
      });
    } else {
      console.log('Error triggering pusher')
    }
    
  })
})

// Middlewares

app.use(express.json());
app.use(cors());

// API config

app.get('/', (req, res) => {
  res.status(200).send('hello');
})

app.get('/messages/sync', (req, res) => {
  Messages.find((err, data) => {
    if(err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(data)
    }
  })
})

app.post('/messages/new', (req, res) => {
  const dbMessage = req.body

  Messages.create(dbMessage, (err, data) => {
    if(err) {
      res.status(500).send(err);
    } else {
      res.status(201).send(data)
    }
  })
})

app.listen(PORT, () => {
  console.log('Listening on port 3333...')
})